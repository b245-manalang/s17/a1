// console.log("Hello World");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function printUserDetails(){
		let fullname = prompt("What is your name?"); 
		let age = prompt("How old are you?"); 
		let location = prompt("Where do you live?");
		alert("Thank you for your input!");
	
		console.log("Hello, " +fullname);
		console.log("You are " +age+ "years old");
		console.log("You live in " +location); 
 	}
 		printUserDetails();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

 		function showNames(){
 			var function1 = "1. Ben&Ben";
 			const function2 = "2. Mayonnaise";
 			let function3 = "3. Parokya ni Edgar";
 			var function4 = "4. Itchyworms";
 			const function5 = "5. Eraserheads";

 			console.log(function1);
 			console.log(function2);
 			console.log(function3);
 			console.log(function4);
 			console.log(function5);
 		}

 		showNames();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

 		function printMyTopMovies(){
 			console.log("1. M3GAN (2022)");
 			console.log("Rotten Tomatoes Rating: 95%");
 			console.log("2. The Menu (2022)");
 			console.log("Rotten Tomatoes Rating: 88%");
 			console.log("3. Puss in Boots: The Last Wish (2022)");
 			console.log("Rotten Tomatoes Rating: 95%");
 			console.log("4. Avatar: The Way of Water (2022)");
 			console.log("Rotten Tomatoes Rating: 77%");
 			console.log("5. Plane (2023)");
 			console.log("Rotten Tomatoes Rating: 76%");

 		}

 			printMyTopMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1);
	console.log(friend2); 
	console.log(friend3); 
};

	printFriends();

// console.log(friend1);
// console.log(friend2);